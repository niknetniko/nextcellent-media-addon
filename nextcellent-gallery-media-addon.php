<?php
/**
 * Plugin Name:     NextCellent Media Library Addon
 * Plugin URI:      https://bitbucket.org/niknetniko/nextcellent-media-addon
 * Description:     Add WP media library integration to NextCellent.
 * Version:         2.1.0
 * Author:          niknetniko
 * Text Domain:     nextcellent-gallery-media-addon
 * Domain Path:     /lang
 * License:         GPL-2.0+
 *
 * This file is part of NextCellent Media Library Addon.
 *
 * NextCellent Media Library Addon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * NextCellent Media Library Addon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NextCellent Media Library Addon.  If not, see <http://www.gnu.org/licenses/>.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' )) {
    die;
}

if(!class_exists('Admin_Error')) {
	require_once( __DIR__ . '/src/lib/ngg-admin-error.php' );
}

function nextcellent_gallery_media_library_addon()
{
    //Check the PHP version.
    if (version_compare( PHP_VERSION, '5.4', '<' )) {
	    $notice = new Admin_Error( printf( __( 'NCG Media Library requires PHP 5.4 or later (you have version %s).', 'nextcellent-gallery-media-addon' ), phpversion() ), 'error' );
	    $notice->show();
    }

	$url = plugin_dir_url( __FILE__ );

    //After the plugin is loaded, we load translations
    load_plugin_textdomain( 'nextcellent-gallery-media-addon', false, basename( dirname( __FILE__ ) ) . '/lang' );

    include_once( 'src/Bootstrap.php' );

    $bootstrap = new Nextcellent\MediaLibraryAddon\Bootstrap($url);
    $bootstrap->start();
}

add_action( 'plugins_loaded', 'nextcellent_gallery_media_library_addon' );
